# This image provides a base for building and running WildFly applications.
# It builds using maven and runs the resulting artifacts on WildFly 10.1.0 Final

FROM openshift/wildfly-101-centos7

MAINTAINER Jose Andres Cordero Benitez <j.cordero@cern.ch>

EXPOSE 8080

# Add customizations
ADD ./contrib/wfmodules/ /wildfly/modules/
ADD ./contrib/wfcfg/standalone.xml /wildfly/standalone/configuration/standalone.xml
ADD ./contrib/wfcfg/standalone-full.xml /wildfly/standalone/configuration/standalone-full.xml

USER 1001

